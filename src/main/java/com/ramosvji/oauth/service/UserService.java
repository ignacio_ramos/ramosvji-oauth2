package com.ramosvji.oauth.service;

import com.ramosvji.oauth.dao.Client;

public interface UserService {
	
	public Client findByUsername(String username);

}
