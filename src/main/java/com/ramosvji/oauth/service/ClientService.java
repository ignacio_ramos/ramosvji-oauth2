package com.ramosvji.oauth.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ramosvji.oauth.client.ClientFeignClient;
import com.ramosvji.oauth.dao.Client;

@Service
public class ClientService implements UserService, UserDetailsService {
	
	private Logger log = LoggerFactory.getLogger(ClientService.class);
	
	@Autowired
	private ClientFeignClient clientFeignClient;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Client client = clientFeignClient.findByUsername(username);
		
		if(client == null) {
			log.error("Error en el login, no existe el usuario '"+username+"' en el sistema");
			throw new UsernameNotFoundException("Error en el login, no existe el usuario '"+username+"' en el sistema");
		}
		
		List<GrantedAuthority> authorities = client.getRoles()
				.stream()
				.map(rol -> new SimpleGrantedAuthority(rol.getName()))
				.peek(authority -> log.info("Role: " + authority.getAuthority()))
				.collect(Collectors.toList());
		
		log.info("Usuario autenticado: " + username);
		
		return new User(client.getUsername(), client.getPassword(), client.isEnable(), true, true, true, authorities);
	}

	@Override
	public Client findByUsername(String username) {
		return clientFeignClient.findByUsername(username);
	}

}
