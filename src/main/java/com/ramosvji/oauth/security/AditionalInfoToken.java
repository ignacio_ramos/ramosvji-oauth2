package com.ramosvji.oauth.security;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import com.ramosvji.oauth.dao.Client;
import com.ramosvji.oauth.service.UserService;

@Component
public class AditionalInfoToken implements TokenEnhancer {
	
	@Autowired
	UserService userService;
	

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		Map<String, Object> info = new HashMap<String, Object>();
		
		Client client = userService.findByUsername(authentication.getName());
		info.put("name", client.getName());
		info.put("lastname", client.getLastname());
		info.put("email", client.getEmail());
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
		
		return accessToken;
	}

}
