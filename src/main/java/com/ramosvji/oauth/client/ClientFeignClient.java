package com.ramosvji.oauth.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ramosvji.oauth.dao.Client;

@FeignClient(name="ramosvji-clients")
public interface ClientFeignClient {
	
	@GetMapping("/ramosvji/v01/clients/{username}")
	public Client findByUsername(@RequestParam String username);

}
